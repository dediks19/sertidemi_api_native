<?php
require_once('../config/koneksi.php');
require 'PHPMailerold/PHPMailerAutoload.php';
include "response.php";
$response = new Response();

$email_login            = $_POST['email_login'];
$url_cek_token          = 'http://devsertidemi.andipublisher.com/api/email/url_verifikasi_email.php?action=cek_token&token=' . $email_login;

$cek_email = mysqli_query($conn, "SELECT login_email FROM user WHERE login_email = '$email_login'")->num_rows;
if ($cek_email == 0) {
  $response->code = 400;
  $response->message = 'Email not found.';
  $response->data = '';
  $response->json();
  die();
} else {

  $cek_email_aktif = mysqli_query($conn, "SELECT login_email FROM user WHERE login_email = '$email_login' AND status_aktif = 'Y'")->num_rows;
  if ($cek_email_aktif > 0){
        $response->code = 400;
        $response->message = 'your email is active.';
        $response->data = '';
        $response->json();
        die();
  } else {

  $mail = new PHPMailer;
  $mail->isSMTP();
  $mail->SMTPOptions = array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
    )
  );
  $mail->Host = 'mail.sertidemi.com';
  $mail->Port = 587;
  $mail->SMTPSecure = 'tsl';
  $mail->SMTPAuth = true;
  $mail->Username = 'admin@sertidemi.com';
  $mail->Password = 'A123123123b@';
  $mail->setFrom('admin@sertidemi.com', 'Sertidemi');
  $mail->addAddress($email_login, $email_login);
  $mail->isHTML(true);
  $mail->Subject = 'Email Re-Verification';
  $mail->Body = '
  <!DOCTYPE html>
  <html>
  <head>
  <title>Email Re-Verification</title>
  <!-- FONT -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
  </head>
  <body style="background:#f3f2ef;font-style: oblique;">
  <div class="email-check" style="max-width:500px; margin:50px auto; padding:20px; background:#fff;border-radius:3px; -webkit-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75); box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.3);">
  <div class="email-container">
  <center><h3>EMAIL VERIFICATION</h3></center>
  <hr><br>
  Hallo, ' . $email_login . '.
  <br><br>
  <div align="justify">
  Congratulations! You have registered for membership <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
  Thank you for registering as a member <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
  Click the link below to activate your account :
  <br><br>
  </div>
  <center>
  <a href="' . $url_cek_token . '" class="text-white btn-success" style="padding: 8px 18px; background-color: #FC4F4F; border: none;border-radius: 5px; font-weight: bold; color: white;" target="__blank" >Email Verification
  </a><br>
  </center>
  <br>
  <center>
  Click the link below if the verification button above does not work.<br><br>
  <a href="' . $url_cek_token . '">https://devsertidemi.andipublisher.com/api/email/url_verifikasi_email.php?action=cek_token&token=' . $email_login . '</a></center><br><br>
  For more information about our services :<br>
  <table style="margin-left: 25px;">
  <tr>
  <td style="padding-right: 15px" nowrap=""> 
  Call Center
  </td>
  <td>
  : +62811-2845-174
  </td>
  </tr>
  <tr>
  <td> 
  Website
  </td>
  <td>
  : www.sertidemi.com
  </td>
  </tr>
  <tr>
  <td style="padding-right: 15px"> 
  Address
  </td>
  <td>
  : Jl. Beo No.38-40, Mrican, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281
  </td>
  </tr>
  </table><br>
  <div style="text-align: left">
  Best regards,<br><br><br>
  <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
  </div>
  </div>
  </div>
  </body>
  </html>';
    }
 }

if (!$mail->send()) {
  $response->code = 400;
  $response->message = 'Email not sent.';
  $response->data = '';
  $response->json();
  die();
} else {
  $response->code = 200;
  $response->message = 'Yes, your registration was successful. Please verify your account first. Check in the Inbox or Spam Email.';
  $response->data = '';
  $response->json();
  die();
}

mysqli_close($conn);
