<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$tag = $_GET['tag'] ?? sendError();
$iduser = $_GET['iduser'] ?? '';
$id = $_GET['id'] ?? '';

$result = '';
switch (strtolower($tag)) {
    case 'event':
        $result = empty($id) ? getEventList($iduser) : getEventDetail($id);
        break;

    case 'assessment':
        $result = empty($id) ? getAssessmentList($iduser) : getAssesstmentDetail($id);
        break;
    default:
        sendError();
        break;
}

$response->code = 200;
$response->message = '';
$response->data = $result;
$response->json();
die();

function sendError()
{
    $response = new Response();
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
}

function getEventList($iduser)
{
    require_once('../config/koneksi.php');
    $result = array();
    $data = $GLOBALS['conn']->query("SELECT *, tt.tanggal_input as tanggal_inputs  FROM transaksi tt
        JOIN transaksi_detail td ON tt.idtransaksi = td.idtransaksi
        JOIN event ev ON td.idevent = ev.idevent
        WHERE tt.iduser = '$iduser' AND tt.jenis_transaksi = 'event' AND ev.event_selesai >= CURRENT_TIMESTAMP AND tt.idtransaksi NOT IN (
        SELECT idtransaksi FROM transaksi WHERE status_transaksi = '8' OR status_transaksi = '9' OR (status_transaksi = '1' AND batas_pembayaran <= CURRENT_TIMESTAMP)
        ) ORDER BY tt.tanggal_input DESC;")->fetch_all(MYSQLI_ASSOC);
    foreach ($data as $key => $value) {
        if ($value['potongan_voucher'] != 0) {
            $total_pembayaran = $value['total_harga_ticket_akhir'];
        } else {
            $total_pembayaran = $value['total_pembayaran'];
        }
        array_push($result, array(
            'idtransaksi' => $value['idtransaksi'],
            'invoice' => $value['invoice'],
            'tanggal_input' => $value['tanggal_inputs'],
            'status_transaksi' => getPayStatus($value['status_transaksi']),
            'nama' => $value['nama_event'],
            'total_pembayaran' => $total_pembayaran,
        ));
    }
    return $result;
}

function getEventDetail($id)
{
    require_once('../config/koneksi.php');
    $data = mysqli_fetch_object($GLOBALS['conn']->query("
    SELECT *, tr.tanggal_input as ttanggal FROM transaksi tr
LEFT JOIN transaksi_detail td ON tr.idtransaksi = td.idtransaksi 
LEFT JOIN event ev ON td.idevent = ev.idevent
LEFT JOIN payments pa ON tr.idpayment = pa.idpayment
WHERE tr.idtransaksi = '$id';
    "));

    if ($data->potongan_voucher != 0) {
        $total_harga_ticket_akhir = $data->total_harga_ticket_akhir;
    } else {
        $total_harga_ticket_akhir = $data->total_pembayaran;
    }
    $invoice = $data->invoice;

    if ($data->status_transaksi == '2') {
        $metode_payment = $data->metode_payment;
        $nomor_payment = $data->nomor_payment;
        $penerima_payment = $data->penerima_payment;
    } else {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sandbox.midtrans.com/v2/" . $invoice . "/status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . base64_encode('SB-Mid-server-mISn2V21ibCM25BFcz7OWeRG'),
                'Content-Type: application/json'
            ),
        ));

        $response_curl = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response_curl, true);

        $payment_midtrans = $response['payment_type'];
        switch ($payment_midtrans) {
            case "gopay":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "shopeepay":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "bank_transfer":
                $res['va_numbers'] = $response['va_numbers'];

                $result2 = array();
                foreach ($response['va_numbers'] as $value) {
                    array_push($result2, array(
                        'bank' => $value['bank'],
                        'va_number' => $value['va_number'],
                    ));
                }

                $metode_payment =  $value['bank'];
                $nomor_payment = $value['va_number'];
                $penerima_payment = '';
                break;
            case "echannel":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "bca_klikpay":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "bca_klikbca":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "cstore":
                $metode_payment =  $response['store'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
        }
    }

    $result['idtransaksi'] = $data->idtransaksi;
    $result['invoice'] = $data->invoice;
    $result['nama_sertifikat'] = $data->nama_sertifikat;
    $result['deskripsi'] = $data->deskripsi_event;
    $result['jenis_transaksi'] = $data->jenis_transaksi;
    $result['status_transaksi'] = getPayStatus($data->status_transaksi);
    $result['batas_pembayaran'] = $data->batas_pembayaran;
    $result['tanggal_input'] = $data->ttanggal;
    $result['total_pembayaran'] = $data->total_pembayaran;
    $result['potongan_voucher'] = (string)$data->potongan_voucher;
    $result['total_harga_ticket_akhir'] = $total_harga_ticket_akhir;
    $result['nama'] = $data->nama_event;
    $result['url_image_panjang'] = $GLOBALS['getimageevent'] . $data->url_image_panjang;
    $result['url_image_kotak'] = $GLOBALS['getimageevent'] . $data->url_image_kotak;
    $result['payment_type'] = $metode_payment;
    $result['nomor_payment'] = $nomor_payment;
    $result['penerima_payment'] = $penerima_payment;
    $result['tanggal_dibayar'] = $data->tanggal_dibayar;

    return $result;
}

function getAssessmentList($iduser)
{
    require_once('../config/koneksi.php');
    $result = array();
    $data = $GLOBALS['conn']->query("SELECT * , tt.tanggal_input as tanggal_inputs FROM transaksi tt
    JOIN transaksi_detail td ON tt.idtransaksi = td.idtransaksi
    JOIN assessment ass ON td.idassessment = ass.idassessment
    WHERE tt.iduser = '$iduser' AND tt.jenis_transaksi = 'assessment' AND ass.tanggal_batas_assessment >= CURRENT_TIMESTAMP AND tt.idtransaksi NOT IN (
    SELECT idtransaksi FROM transaksi WHERE status_transaksi = '8' OR status_transaksi = '9' OR (status_transaksi = '1' AND batas_pembayaran <= CURRENT_TIMESTAMP)) ORDER BY tt.tanggal_input DESC")->fetch_all(MYSQLI_ASSOC);
    foreach ($data as $key => $value) {
        if ($value['potongan_voucher'] != 0) {
            $total_pembayaran = $value['total_harga_ticket_akhir'];
        } else {
            $total_pembayaran = $value['total_pembayaran'];
        }
        array_push($result, array(
            'idtransaksi' => $value['idtransaksi'],
            'invoice' => $value['invoice'],
            'tanggal_input' => $value['tanggal_inputs'],
            'status_transaksi' => getPayStatus($value['status_transaksi']),
            'nama' => $value['nama_assessment'],
            'total_pembayaran' => $total_pembayaran,
        ));
    }
    return $result;
}


function getAssesstmentDetail($id)
{
    require_once('../config/koneksi.php');
    $data = mysqli_fetch_object($GLOBALS['conn']->query("
    SELECT *, tr.tanggal_input as ttanggal FROM transaksi tr
LEFT JOIN transaksi_detail td ON tr.idtransaksi = td.idtransaksi 
LEFT JOIN assessment ass ON td.idassessment = ass.idassessment
LEFT JOIN payments pa ON tr.idpayment = pa.idpayment
WHERE tr.idtransaksi = '$id';
    "));

    if ($data->potongan_voucher != 0) {
        $total_harga_ticket_akhir = $data->total_harga_ticket_akhir;
    } else {
        $total_harga_ticket_akhir = $data->total_pembayaran;
    }
    $invoice = $data->invoice;

    if ($data->status_transaksi == '2') {
        $metode_payment = $data->metode_payment;
        $nomor_payment = $data->nomor_payment;
        $penerima_payment = $data->penerima_payment;
    } else {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sandbox.midtrans.com/v2/" . $invoice . "/status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . base64_encode('SB-Mid-server-mISn2V21ibCM25BFcz7OWeRG'),
                'Content-Type: application/json'
            ),
        ));

        $response_curl = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response_curl, true);
        $payment_midtrans = $response['payment_type'];
        switch ($payment_midtrans) {
            case "gopay":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "shopeepay":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "bank_transfer":
                $res['va_numbers'] = $response['va_numbers'];

                $result2 = array();
                foreach ($response['va_numbers'] as $value) {
                    array_push($result2, array(
                        'bank' => $value['bank'],
                        'va_number' => $value['va_number'],
                    ));
                }

                $metode_payment =  $value['bank'];
                $nomor_payment = $value['va_number'];
                $penerima_payment = '';
                break;
            case "echannel":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "bca_klikpay":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "bca_klikbca":
                $metode_payment =  $response['payment_type'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
            case "cstore":
                $metode_payment =  $response['store'];
                $nomor_payment = $response['transaction_id'];
                $penerima_payment = '';
                break;
        }
    }

    $result['idtransaksi'] = $data->idtransaksi;
    $result['invoice'] = $data->invoice;
    $result['nama_sertifikat'] = $data->nama_sertifikat;
    $result['deskripsi'] = $data->deskripsi_assessment;
    $result['jenis_transaksi'] = $data->jenis_transaksi;
    $result['status_transaksi'] = getPayStatus($data->status_transaksi);
    $result['batas_pembayaran'] = $data->batas_pembayaran;
    $result['tanggal_input'] = $data->ttanggal;
    $result['total_pembayaran'] = $data->total_pembayaran;
    $result['potongan_voucher'] = (string)$data->potongan_voucher;
    $result['total_harga_ticket_akhir'] = $total_harga_ticket_akhir;
    $result['nama'] = $data->nama_assessment;
    $result['url_image_panjang'] = $GLOBALS['getimageassessment'] . $data->url_image_panjang;
    $result['url_image_kotak'] = $GLOBALS['getimageassessment'] . $data->url_image_kotak;
    $result['payment_type'] = $metode_payment;
    $result['nomor_payment'] = $nomor_payment;
    $result['penerima_payment'] = $penerima_payment;
    $result['tanggal_dibayar'] = $data->tanggal_dibayar;

    return $result;
}

function getPayStatus($number)
{
    switch ($number) {
        case '1':
            $status_transaksi = "Waiting For Payment";
            break;
        case '2':
            $status_transaksi = "Waiting for Payment Verification";
            break;
        case '3':
            $status_transaksi = "Payment Successfully";
            break;
        case '4':
            $status_transaksi = "Incomplete payment";
            break;
        case '5':
            $status_transaksi = "Sending";
            break;
        case '6':
            $status_transaksi = "Received";
            break;
        case '7':
            $status_transaksi = "Transaction Complete";
            break;
        case '8':
            $status_transaksi = "Expired";
            break;
        case '9':
            $status_transaksi = "Canceled";
            break;
        case '10':
            $status_transaksi = "Payment declined";
            break;
        default:
            $status_transaksi = "Not defined";
            break;
    }
    return $status_transaksi;
}
