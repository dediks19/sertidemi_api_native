<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$iduser  = $_POST['iduser'] ?? '';
$idvoucher  = $_POST['idvoucher'] ?? '';

$cekkuota = mysqli_query($conn, "SELECT * FROM transaksi_voucher_mandiri WHERE idtransaksi_voucher_mandiri = '$idvoucher' AND qty_voucher_sisa != 0")->num_rows;
$cekvoucher = mysqli_query($conn, "SELECT * FROM `user_voucher_mandiri` WHERE idtransaksi_voucher_mandiri = '$idvoucher' AND iduser = '$iduser'")->num_rows;
$getkuota = mysqli_query($conn, "SELECT qty_voucher_sisa FROM transaksi_voucher_mandiri WHERE idtransaksi_voucher_mandiri = '$idvoucher'")->fetch_assoc();
$stok = $getkuota['qty_voucher_sisa'];

if ($cekkuota > 0) {
    if ($cekvoucher > 0) {
        $response->code = 200;
        $response->message = 'This voucher has been claimed.';
        $response->data = [];
        $response->json();
        die();
    } else {
        $conn->begin_transaction();

        $query[] = $conn->query("INSERT into user_voucher_mandiri SET iduser_voucher_mandiri = UUID_SHORT(),
        iduser = '$iduser',
        idtransaksi_voucher_mandiri = '$idvoucher',
        tanggal_input = CURRENT_TIME()");

        $query[] = $conn->query("UPDATE transaksi_voucher_mandiri SET qty_voucher_sisa = $stok-1 WHERE idtransaksi_voucher_mandiri = '$idvoucher'");

        if (in_array(false, $query)) {
            $response->code = 200;
            $response->message = 'Voucher failed to claim.';
            $response->data = '';
            $response->json();
            die();
        } else {
            $conn->commit();
            $response->code = 200;
            $response->message = 'The voucher was successfully claimed.';
            $response->data = '';
            $response->json();
            die();
        }
    }
} else {
    $response->code = 200;
    $response->message = 'vouchers no longer exist';
    $response->data = [];
    $response->json();
    die();
}
