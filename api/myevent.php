<?php
require_once "../config/koneksi.php";
include "response.php";

$iduser = $_GET['iduser'] ?? '';
$idevent = $_GET['idevent'] ?? '';


$response = new Response();
if (!empty($idevent)) {
    $data = mysqli_fetch_object($conn->query("SELECT * FROM event e
    JOIN transaksi_detail td ON e.idevent = td.idevent 
    JOIN transaksi t ON td.idtransaksi = t.idtransaksi
    LEFT JOIN user_bo bo ON e.iduser_bo = bo.iduser_bo
    LEFT JOIN kategori_user_bo c ON b.idkategori = c.idkategori
    WHERE e.idevent = '$idevent' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= e.event_selesai"));
    if (is_null($data)) {
        $response->code = 200;
        $response->message = "data empty";
        $response->data = '';
        $response->json();
        die();
    }

    $listsponsor = array();
    $sponsores = $conn->query("SELECT * FROM sponsor WHERE idevent = '$idevent'");
    foreach ($sponsores as $key => $value) {
        array_push($listsponsor, array(
            'idsponsor' => $value['idsponsor'],
            'nama_sponsor' => $value['nama_sponsor'],
            'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
        ));
    }

    // $sponsores = $conn->query("SELECT * FROM sponsor WHERE idevent = '$data->idevent';")->fetch_all(MYSQLI_ASSOC);

    $result['idevent']          = $data->idevent;
    $result['nama_event']       = $data->nama_event;
    $result['idevent_kategori'] = $data->idevent_kategori;
    $result['deskripsi_event']  = $data->deskripsi_event;
    $result['link_status']      = $data->link_status;
    $result['link_meeting']     = $data->link_meeting;
    $result['event_mulai']      = $data->event_mulai;
    $result['event_selesai']    = $data->event_selesai;
    $result['tanggal_input']    = $data->tanggal_input;
    $result['url_image_panjang'] = $getimageevent . $data->url_image_panjang;
    $result['url_image_kotak']  = $getimageevent . $data->url_image_kotak;
    $result['harga']            = $data->harga;
    $result['diskon']           = $data->diskon;
    $result['status_diskon']    = $data->status_diskon;
    $result['penulis']          = $data->nama_kategori;
    $result['sponsor']          = $listsponsor;
    $result['nama_daftar']      = $data->nama_sertifikat;
    $result['point']            = $data->point_event." ".$data->point_event_nama;

    $response->code = 200;
    $response->message = 'found';
    $response->data = $result;
    $response->json();
    die();
} else {
    $data = $conn->query("SELECT *, (SELECT COUNT(event_materi.idevent_materi) FROM event_materi WHERE event_materi.idevent = e.idevent) AS materi_count, e.idassessment as idassessments FROM event e
    JOIN transaksi_detail td ON e.idevent = td.idevent 
    JOIN transaksi t ON td.idtransaksi = t.idtransaksi
    WHERE t.iduser = '$iduser' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= e.event_selesai ORDER BY t.tanggal_input DESC");

    $result = array();

    foreach ($data as $key => $value) {
        switch ($value['link_status']) {
            case '2':
                $link_status = "Zoom";
                break;
            case '3':
                $link_status = "Gmeet";
                break;
            default:
                $link_status = "Not Yet Available";
                break;
        }
        array_push($result, array(
            'idevent' => $value['idevent'],
            'nama_event' => substr($value['nama_event'], 0, 27),
            'nama_daftar' => $value['nama_sertifikat'],
            'event_selesai' => $value['event_selesai'],
            'event_mulai' => $value['event_mulai'],
            'assessment' => empty($value['idassessments']) ? false : true,
            'idassessment' => $value['idassessments'],
            'url_image_panjang' => $getimageevent . $value['url_image_panjang'],
            'url_image_kotak' => $getimageevent . $value['url_image_kotak'],
            'deskripsi_event' => substr($value['deskripsi_event'], 0, 27),
            'link_status' => $link_status,
            'link_meeting' => $value['link_meeting'],
            'materi_count' => $value['materi_count'],
        ));
    }

    $response->code = 200;
    $response->message = "found";
    $response->data = $result;
    $response->json();
    die();
}

function switchStatus($status)
{
    switch ($status) {
        case '1':
            $status_transaksi = "Waiting For Payment";
            break;
        case '2':
            $status_transaksi = "Waiting for Payment Verification";
            break;
        case '3':
            $status_transaksi = "Payment Successfully";
            break;
        case '4':
            $status_transaksi = "Incomplete payment";
            break;
        case '5':
            $status_transaksi = "Sending";
            break;
        case '6':
            $status_transaksi = "Received";
            break;
        case '7':
            $status_transaksi = "Transaction Complete";
            break;
        case '8':
            $status_transaksi = "Expired";
            break;
        case '9':
            $status_transaksi = "Canceled";
            break;
        case '10':
            $status_transaksi = "Payment declined";
            break;
        default:
            $status_transaksi = "Not defined";
            break;
    }
    return $status_transaksi;
}
