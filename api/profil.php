<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$id_user                = $_POST['id'] ?? sendError('bad request');

$data = mysqli_fetch_object($conn->query("SELECT iduser, nama, telp, url_image FROM user WHERE iduser = '$id_user'")) ?? sendError('ID not valid');

$no_hp                  = $_POST['telp'] ?? $data->telp;
$nama                   = $_POST['nama'] ?? $data->nama;

$no_hp      = $data->telp == $no_hp ? $data->telp : $_POST['telp'];
$nama       = $data->nama == $nama ? $data->nama : $_POST['nama'];

if (!empty($_FILES['uploadedfile'])) {
    if (isset($_FILES['uploadedfile']['type'])) {

        $nama_file  = random_word(10) . ".png";
        $lokasi     = $_FILES['uploadedfile']['tmp_name'];

        if (move_uploaded_file($lokasi, "../image/user/" . $nama_file)) {

            if (isset($_POST['pass_login'])) {
                $pass_login = password_hash($_POST['pass_login'], PASSWORD_DEFAULT);
                $query = mysqli_query($conn, "UPDATE user SET nama = '$nama', telp = '$no_hp', login_password = '$pass_login', url_image = '$nama_file' WHERE iduser = '$id_user'");

                if ($query) {
                    file_exists("../image/user/" . $data->url_image) ? unlink("../image/user/" . $data->url_image) : '';

                    $result['iduser'] = $data->iduser;
                    $result['nama'] = $nama;
                    $result['telp'] = $no_hp;
                    $result['url_image'] = $getprofile . $nama_file;

                    $response->code = 200;
                    $response->message = "Profil kamu berhasil diperbarui\n\nKlik `OK` untuk menutup pemberitahuan ini.";
                    $response->data = $result;
                    $response->json();
                    die();
                } else {
                    unlink("../image/user/" . $nama_file);
                    sendError("Gagal edit profil!\nKlik `Mengerti` untuk menutup pesan ini");
                }
            } else {
                $query = mysqli_query($conn, "UPDATE user SET nama = '$nama', telp = '$no_hp', url_image = '$nama_file' WHERE iduser = '$id_user'");

                if ($query) {
                    file_exists("../image/user/" . $data->url_image) ? unlink("../image/user/" . $data->url_image) : '';

                    $result['iduser'] = $data->iduser;
                    $result['nama'] = $nama;
                    $result['telp'] = $no_hp;
                    $result['url_image'] = $getprofile . $nama_file;

                    $response->code = 200;
                    $response->message = "Profil kamu berhasil diperbarui\n\nKlik `OK` untuk menutup pemberitahuan ini.";
                    $response->data = $result;
                    $response->json();
                    die();
                } else {
                    unlink("../image/user/" . $nama_file);
                    sendError("Gagal edit profil!\nKlik `Mengerti` untuk menutup pesan ini");
                }
            }
        } else {
            sendError("Upload file mengalami kegagalan!\nKlik `Mengerti` untuk menutup pesan ini");
        }
    } else {
        sendError("Format tidak diperbolehkan!\nKlik `Mengerti` untuk menutup pesan ini");
    }
} else {

    if (isset($_POST['pass_login'])) {
        $pass_login = password_hash($_POST['pass_login'], PASSWORD_DEFAULT);
        $query = mysqli_query($conn, "UPDATE user SET nama = '$nama', telp = '$no_hp', login_password = '$pass_login' WHERE iduser = '$id_user'");

        if ($query) {
            $result['iduser'] = $data->iduser;
            $result['nama'] = $nama;
            $result['telp'] = $no_hp;
            $result['url_image'] = $getprofile . $data->url_image;

            $response->code = 200;
            $response->message = "Profil kamu berhasil diperbarui\n\nKlik `OK` untuk menutup pemberitahuan ini.";
            $response->data = $result;
            $response->json();
            die();
        } else {
            sendError("Gagal edit profil!\nKlik `Mengerti` untuk menutup pesan ini");
        }
    } else {
        $query = mysqli_query($conn, "UPDATE user SET nama = '$nama', telp = '$no_hp' WHERE iduser = '$id_user'");

        if ($query) {
            $result['iduser'] = $data->iduser;
            $result['nama'] = $nama;
            $result['telp'] = $no_hp;
            $result['url_image'] = $getprofile . $data->url_image;

            $response->code = 200;
            $response->message = "Profil kamu berhasil diperbarui\n\nKlik `OK` untuk menutup pemberitahuan ini.";
            $response->data = $result;
            $response->json();
            die();
        } else {
            sendError("Gagal edit profil!\nKlik `Mengerti` untuk menutup pesan ini");
        }
    }
}

mysqli_close($conn);

function sendError($msg)
{
    $response = new Response();
    $response->code = 400;
    $response->message = $msg;
    $response->data = '';
    $response->json();
    die();
}
