<?php
require_once('../config/koneksi.php');
require 'PHPMailerold/PHPMailerAutoload.php';
include "response.php";
$response = new Response();

$email_login            = $_POST['email_login'];
$telp                   = $_POST['no_hp'];
$nama                   = $_POST['nama'];
$alamat                 = $_POST['alamat'];
$pass_login             = password_hash($_POST['pass_login'], PASSWORD_DEFAULT);
$url_cek_token          = 'http://devsertidemi.andipublisher.com/api/email/url_verifikasi_email.php?action=cek_token&token=' . $email_login;
$getreferalcode = generate_referal_lagi();

if (empty($_POST['referal'])) {
  $fkreferal = '';
} else {
  $fkreferal = $_POST['referal'];
  $cekreferal = mysqli_query($conn, "SELECT myreferal_code FROM user WHERE myreferal_code = '$fkreferal' AND status_aktif  = 'Y'")->num_rows;
  if ($cekreferal == 0) {
    $response->code = 400;
    $response->message = 'No referral number!';
    $response->data = '';
    $response->json();
    die();
  }
}

if (!empty($_FILES['uploadedfile'])) {
  if (isset($_FILES['uploadedfile']['type'])) {

    $nama_file  = random_word(10) . ".png";
    $lokasi     = $_FILES['uploadedfile']['tmp_name'];

    if (move_uploaded_file($lokasi, "../image/user/" . $nama_file)) {
      $cek_email = mysqli_query($conn, "SELECT login_email FROM user WHERE login_email = '$email_login'")->num_rows;
      $cek_hp = mysqli_query($conn, "SELECT telp FROM user WHERE telp = '$telp'")->num_rows;

      if ($cek_email > 0) {
        $response->code = 400;
        $response->message = 'Email already exists, please re-verify your email.';
        $response->data = '';
        $response->json();
        die();
      } else {
        if ($cek_hp > 0) {
          $response->code = 400;
          $response->message = 'The phone number is already in use, please use a different phone number.';
          $response->data = '';
          $response->json();
          die();
        } else {
          $iduser = createID('iduser', 'user', 'US');
          $referal = generate_referal_lagi();
          $query = mysqli_query($conn, "INSERT INTO `user`(`iduser`, `nama`, `login_email`, `telp`, `alamat`, `login_password`, `url_image`, `kode_referal`, `myreferal_code`)
          VALUES ('$iduser', '$nama', '$email_login', '$telp', '$alamat', '$pass_login', '$nama_file', '$fkreferal', '$getreferalcode')");

          $data1['iduser'] = $iduser;
          $data1['login_email'] = $email_login;

          if ($query) {

            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPOptions = array(
              'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
              )
            );
            $mail->Host = 'mail.sertidemi.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tsl';
            $mail->SMTPAuth = true;
            $mail->Username = 'admin@sertidemi.com';
            $mail->Password = 'A123123123b@';
            $mail->setFrom('admin@sertidemi.com', 'Sertidemi');
            $mail->addAddress($email_login, $nama);
            $mail->isHTML(true);
            $mail->Subject = 'Email Verification';
            $mail->Body = '
          <!DOCTYPE html>
          <html>
          <head>
          <title>Email Verification</title>
          <!-- FONT -->
          <link rel="preconnect" href="https://fonts.gstatic.com">
          <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
          </head>
          <body style="background:#f3f2ef;font-style: oblique;">
          <div class="email-check" style="max-width:500px; margin:50px auto; padding:20px; background:#fff;border-radius:3px; -webkit-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75); box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.3);">
          <div class="email-container">
          <center><h3>EMAIL VERIFICATION</h3></center>
          <hr><br>
          Hallo, ' . $email_login . '.
          <br><br>
          <div align="justify">
          Congratulations! You have registered for membership <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
          Thank you for registering as a member <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
          Click the link below to activate your account :
          <br><br>
          </div>
          <center>
          <a href="' . $url_cek_token . '" class="text-white btn-success" style="padding: 8px 18px; background-color: #FC4F4F; border: none;border-radius: 5px; font-weight: bold; color: white;" target="__blank" >Email Verification
          </a><br>
          </center>
          <br>
          <center>
          Click the link below if the verification button above does not work.<br><br>
          <a href="' . $url_cek_token . '">https://devsertidemi.andipublisher.com/api/email/url_verifikasi_email.php?action=cek_token&token=' . $email_login . '</a></center><br><br>
          For more information about our services :<br>
          <table style="margin-left: 25px;">
          <tr>
          <td style="padding-right: 15px" nowrap=""> 
          Call Center
          </td>
          <td>
          : +62811-2845-174
          </td>
          </tr>
          <tr>
          <td> 
          Website
          </td>
          <td>
          : www.sertidemi.com
          </td>
          </tr>
          <tr>
          <td style="padding-right: 15px"> 
          Address
          </td>
          <td>
          : Jl. Beo No.38-40, Mrican, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281
          </td>
          </tr>
          </table><br>
          <div style="text-align: left">
          Best regards,<br><br><br>
          <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
          </div>
          </div>
          </div>
          </body>
          </html>';

            if (!$mail->send()) {
              $response->code = 400;
              $response->message = 'Email not sent.';
              $response->data = '';
              $response->json();
              die();
            } else {
              $response->code = 200;
              $response->message = 'Thank you for your registration at Sertidemi application. Please check your email on inbox or spam to verify your account.';
              $response->data = $data1;
              $response->json();
              die();
            }
          } else {
            unlink("../image/user/" . $nama_file);
            $response->code = 400;
            $response->message = 'Failed to add new image.';
            $response->data = '';
            $response->json();
            die();
          }
        }
      }
    } else {
      $response->code = 400;
      $response->message = 'File upload has failed.';
      $response->data = '';
      $response->json();
      die();
    }
  } else {
    $response->code = 400;
    $response->message = 'Formats are not allowed.';
    $response->data = '';
    $response->json();
    die();
  }
} else {
  $cek_email = mysqli_query($conn, "SELECT login_email FROM user WHERE login_email = '$email_login'")->num_rows;
  $cek_hp = mysqli_query($conn, "SELECT telp FROM user WHERE telp = '$telp'")->num_rows;

  if ($cek_email > 0) {
    $response->code = 400;
    $response->message = 'Email already exists, please re-verify your email.';
    $response->data = '';
    $response->json();
    die();
  } else {
    if ($cek_hp > 0) {
      $response->code = 400;
      $response->message = 'The phone number is already in use, please use a different phone number.';
      $response->data = '';
      $response->json();
      die();
    } else {
      $iduser = createID('iduser', 'user', 'US');
      $referal = generate_referal_lagi();
      $query = mysqli_query($conn, "INSERT INTO `user`(`iduser`, `nama`, `login_email`, `telp`, `alamat`, `login_password`, `kode_referal`, `myreferal_code`)
        VALUES ('$iduser', '$nama', '$email_login', '$telp', '$alamat', '$pass_login', '$fkreferal', '$getreferalcode')");

      $data1['iduser'] = $iduser;
      $data1['login_email'] = $email_login;

      if ($query) {

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPOptions = array(
          'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
          )
        );
        $mail->Host = 'mail.sertidemi.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tsl';
        $mail->SMTPAuth = true;
        $mail->Username = 'admin@sertidemi.com';
        $mail->Password = 'A123123123b@';
        $mail->setFrom('admin@sertidemi.com', 'Sertidemi');
        $mail->addAddress($email_login, $nama);
        $mail->isHTML(true);
        $mail->Subject = 'Verifikasi E-mail';
        $mail->Body = '
        <!DOCTYPE html>
        <html>
        <head>
        <title>Email Verification</title>
        <!-- FONT -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
        </head>
        <body style="background:#f3f2ef;font-style: oblique;">
        <div class="email-check" style="max-width:500px; margin:50px auto; padding:20px; background:#fff;border-radius:3px; -webkit-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.75); box-shadow: 0px 2px 17px 0px rgba(0,0,0,0.3);">
        <div class="email-container">
        <center><h3>EMAIL VERIFICATION</h3></center>
        <hr><br>
        Hallo, ' . $email_login . '.
        <br><br>
        <div align="justify">
        Congratulations! You have registered for membership <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
        Thank you for registering as a member <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
        Click the link below to activate your account :
        <br><br>
        </div>
        <center>
        <a href="' . $url_cek_token . '" class="text-white btn-success" style="padding: 8px 18px; background-color: #FC4F4F; border: none;border-radius: 5px; font-weight: bold; color: white;" target="__blank" >Email Verification
        </a><br>
        </center>
        <br>
        <center>
        Click the link below if the verification button above does not work.<br><br>
        <a href="' . $url_cek_token . '">https://devsertidemi.andipublisher.com/api/email/url_verifikasi_email.php?action=cek_token&token=' . $email_login . '</a></center><br><br>
        For more information about our services :<br>
        <table style="margin-left: 25px;">
        <tr>
        <td style="padding-right: 15px" nowrap=""> 
        Call Center
        </td>
        <td>
        : +62811-2845-174
        </td>
        </tr>
        <tr>
        <td> 
        Website
        </td>
        <td>
        : www.sertidemi.com
        </td>
        </tr>
        <tr>
        <td style="padding-right: 15px"> 
        Address
        </td>
        <td>
        : Jl. Beo No.38-40, Mrican, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281
        </td>
        </tr>
        </table><br>
        <div style="text-align: left">
        Best regards,<br><br><br>
        <b style="font-family: sans-serif; color:#21325E;"> SERTIDEMI</b>.<br>
        </div>
        </div>
        </div>
        </body>
        </html>';

        if (!$mail->send()) {
          $response->code = 400;
          $response->message = 'Email not sent.';
          $response->data = '';
          $response->json();
          die();
        } else {
          $response->code = 200;
          $response->message = 'Thank you for your registration at Sertidemi application. Please check your email on inbox or spam to verify your account.';
          $response->data = $data1;
          $response->json();
          die();
        }
      } else {
        $response->code = 400;
        $response->message = 'Failed to add new Member.';
        $response->data = '';
        $response->json();
        die();
      }
    }
  }
}
mysqli_close($conn);
