<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$q  = $_GET['q'] ?? '';

if (empty($q)) {
    $datalist = array();
    $data = $conn->query("SELECT * FROM transaksi_voucher_mandiri WHERE tanggal_mulai <= CURRENT_TIME() AND tanggal_selesai >= CURRENT_TIME() AND qty_voucher_sisa != 0  ORDER BY tanggal_input DESC");
} else {
    $datalist = array();
    $data = $conn->query("SELECT * FROM transaksi_voucher_mandiri WHERE nama_voucher LIKE '%$q%' AND tanggal_mulai <= CURRENT_TIME() AND tanggal_selesai >= CURRENT_TIME() AND qty_voucher_sisa != 0 ORDER BY tanggal_input DESC");
}

foreach ($data as $key => $value) {
    array_push($datalist, array(
        'idtransaksi_voucher_mandiri' => $value['idtransaksi_voucher_mandiri'],
        'nama_voucher' => $value['nama_voucher'],
        'deskripsi_voucher' => $value['deskripsi_voucher'],
        'jenis_potongan' => $value['jenis_potongan'],
        'nilai_potongan' => $value['nilai_potongan'],
        'tanggal_mulai' => $value['tanggal_mulai'],
        'tanggal_selesai' => $value['tanggal_selesai'],
        'qty_voucher_sisa' => $value['qty_voucher_sisa'],
    ));
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $datalist;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'No data is displayed.';
    $response->data = [];
    $response->json();
    die();
}