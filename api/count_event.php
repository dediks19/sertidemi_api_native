<?php
require_once "../config/koneksi.php";
include "response.php";

$response = new Response();

$iduser = $_GET['iduser'] ?? '';
if (empty($iduser)) {
    $data2 = mysqli_fetch_object($conn->query("SELECT *, (SELECT COUNT(event_materi.idevent_materi) FROM event_materi WHERE event_materi.idevent = e.idevent) AS materi_count FROM event e
JOIN transaksi_detail td ON e.idevent = td.idevent 
JOIN transaksi t ON td.idtransaksi = t.idtransaksi
WHERE t.iduser = '$iduser' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= e.event_selesai"));
} else {
    $data2 = mysqli_fetch_object($conn->query("SELECT *, (SELECT COUNT(event_materi.idevent_materi) FROM event_materi WHERE event_materi.idevent = e.idevent) AS materi_count FROM event e
    JOIN transaksi_detail td ON e.idevent = td.idevent 
    JOIN transaksi t ON td.idtransaksi = t.idtransaksi
    WHERE t.iduser = '$iduser' AND t.status_transaksi = '7' AND CURRENT_TIMESTAMP() <= e.event_selesai"));
}

$data1 = mysqli_fetch_object($conn->query("SELECT COUNT(dievent_kategori) AS jumlah_kategori FROM event_kategori"));
// $data1 = mysqli_fetch_object($conn->query("SELECT COUNT(dievent_kategori) AS jumlah_kategori FROM event_kategori"));
$data4 = mysqli_fetch_object($conn->query("SELECT COUNT(idevent_sertifikat) AS jumlah_sertifikat FROM event_sertifikat"));

// $sponsores = $conn->query("SELECT * FROM sponsor WHERE idevent = '$data->idevent';")->fetch_all(MYSQLI_ASSOC);
// $banner = $conn->query("SELECT * FROM event_banners WHERE idevent = '$data->idevent'")->fetch_all(MYSQLI_ASSOC);

$result['jumlah_kategori']  = $data1->jumlah_kategori;
$result['jumlah_sertifikat']  = $data4->jumlah_sertifikat;

$response->code = 200;
$response->message = 'found';
$response->data = $result;
$response->json();
die();


function sendError()
{
    $response = new Response();
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
}
