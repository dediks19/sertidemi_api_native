<?php
require_once "../config/koneksi.php";
include "response.php";

$iduser     = $_POST['iduser'] ?? sendError();
$totalbayar = $_POST['totalbayar'] ?? sendError();
$statuspayment = $_POST['status_payment'] ?? sendError();
$idvoucher = $_POST['idvoucher'] ?? '';
$idevent    = $_POST['idevent'] ?? sendError();
$nama_sertifikat    = $_POST['nama_sertifikat'] ?? sendError();

$response = new Response();
$exp_date = date("Y-m-d H:i:s", strtotime("+72 hours"));

$event  = mysqli_fetch_object($conn->query("SELECT * FROM event where idevent =  '$idevent'"));
$transaction = mysqli_fetch_object($conn->query("SELECT UUID_SHORT() as id"));

// var_dump($event);
// die();

if ($exp_date > $event->event_selesai) {
    $exp_date = $event->event_selesai;
}
$conn->begin_transaction();
$invoice = createInvoice('invoice', 'transaksi', 'TR');

if ($totalbayar > 0) {
    if (!empty($idvoucher)) {
        $data[] = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
            invoice = '$invoice',
            jenis_transaksi = 'event',
            iduser = '$iduser',
            status_transaksi = '1',
            status_payment = '$statuspayment',
            batas_pembayaran = '$exp_date',
            total_pembayaran = '$totalbayar',
            idtransaksi_voucher = '$idvoucher',
            nama_sertifikat = '$nama_sertifikat',
            jumlah_point = '$event->point_event',
            nama_point = '$event->point_event_nama'
        ");

        $data[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
            idtransaksi = '$transaction->id',
            idevent = '$idevent',
            harga_normal = '$event->harga',
            status_diskon = '$event->status_diskon',
            diskon = '$event->diskon'
        ");

        $filesertifikat = $getsertifikatevent . "?idtransaksi=" . $transaction->id;

        $data[] = $conn->query("INSERT into event_sertifikat SET idevent_sertifikat = UUID_SHORT(),
        idevent = '$idevent',
        idtransaksi = '$transaction->id',
        status_sertifikat = 'N',
        link_sertifikat = '$filesertifikat'");

        $data[] = $conn->query("INSERT into notifikasi SET idnotifikasi = UUID_SHORT(),
        idtransaksi = '$transaction->id',
        iduser = '$iduser',
        status_baca = 'N'
        ");
    } else {
        $data[] = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
            invoice = '$invoice',
            jenis_transaksi = 'event',
            iduser = '$iduser',
            status_transaksi = '1',
            status_payment = '$statuspayment',
            batas_pembayaran = '$exp_date',
            total_pembayaran = '$totalbayar',
            nama_sertifikat = '$nama_sertifikat',
            jumlah_point = '$event->point_event',
            nama_point = '$event->point_event_nama'
        ");

        $data[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
            idtransaksi = '$transaction->id',
            idevent = '$idevent',
            harga_normal = '$event->harga',
            status_diskon = '$event->status_diskon',
            diskon = '$event->diskon'
        ");

        $filesertifikat = $getsertifikatevent . "?idtransaksi=" . $transaction->id;

        $data[] = $conn->query("INSERT into event_sertifikat SET idevent_sertifikat = UUID_SHORT(),
        idevent = '$idevent',
        idtransaksi = '$transaction->id',
        status_sertifikat = 'N',
        link_sertifikat = '$filesertifikat'");

        $data[] = $conn->query("INSERT into notifikasi SET idnotifikasi = UUID_SHORT(),
        idtransaksi = '$transaction->id',
        iduser = '$iduser',
        status_baca = 'N'
        ");
    }
} else {
    $data[] = $conn->query("INSERT into transaksi SET idtransaksi = '$transaction->id',
            invoice = '$invoice',
            jenis_transaksi = 'event',
            iduser = '$iduser',
            status_transaksi = '7',
            status_payment = '$statuspayment',
            batas_pembayaran = '$exp_date',
            total_pembayaran = '$totalbayar',
            nama_sertifikat = '$nama_sertifikat',
            jumlah_point = '$event->point_event',
            nama_point = '$event->point_event_nama'
        ");

    $data[] = $conn->query("INSERT into transaksi_detail SET idtransaksi_detail = UUID_SHORT(),
            idtransaksi = '$transaction->id',
            idevent = '$idevent',
            harga_normal = '$event->harga',
            status_diskon = '$event->status_diskon',
            diskon = '$event->diskon'
        ");

    $filesertifikat = $getsertifikatevent . "?idtransaksi=" . $transaction->id;

    $data[] = $conn->query("INSERT into event_sertifikat SET idevent_sertifikat = UUID_SHORT(),
    idevent = '$idevent',
    idtransaksi = '$transaction->id',
    status_sertifikat = 'N',
    link_sertifikat = '$filesertifikat'");

    $data[] = $conn->query("INSERT into notifikasi SET idnotifikasi = UUID_SHORT(),
    idtransaksi = '$transaction->id',
    iduser = '$iduser',
    status_baca = 'N'
    ");
}




if (in_array(false, $data)) {
    var_dump($data);
    $response->code = 400;
    $response->message = mysqli_error($conn);
    $response->data = '';
    $response->json();
    die();
} else {
    $trans_data = mysqli_fetch_object($conn->query("SELECT tanggal_input, payment_type FROM transaksi WHERE idtransaksi = '$transaction->id';"));
    $result['id_transaksi'] = $transaction->id;
    $result['no_invoice'] = $invoice;
    $result['total'] = $totalbayar;
    $result['metode_pembayaran'] = empty($trans_data->payment_type) ? 'free' : $trans_data->payment_type;
    $result['nama_event'] = $event->nama_event;
    $result['created_at'] = $trans_data->tanggal_input;

    $conn->commit();
    $response->code = 200;
    $response->message = 'done';
    $response->data = $result;
    $response->json();
    die();
}

function sendError($text)
{
    $response = new Response();
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
}
