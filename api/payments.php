<?php
require_once('../config/koneksi.php');
include "response.php";

$response = new Response();

$data = $conn->query("SELECT * FROM payments");
$data = $data->fetch_all(MYSQLI_ASSOC);

if ($data[0]) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $data ;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'Tidak ada data ditampilkan.';
    $response->data = [];
    $response->json();
    die();
}
