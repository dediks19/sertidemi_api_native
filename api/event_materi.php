<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$idevent                    = $_GET['idevent'] ?? '';
$tag                             = $_GET['tag'];

switch ($tag) {
    case "materi":
        $data = $conn->query("SELECT * FROM event_materi where idevent = '$idevent' AND link_status = '1'");
        $datalist = array();
        foreach ($data as $key => $value) {
            if ($value['link_status'] = '1') {
                $keterangan = 'file pdf/books';
            } else if ($value['link_status'] = '3') {
                $keterangan = 'video';
            } else {
                $keterangan = 'file pdf/books';
            }
            array_push($datalist, array(
                'idevent_materi' => $value['idevent_materi'],
                'link_materi' => $getmateri . $value['link_materi'],
                'link_status' => $value['link_status'],
                'keterangan' => $keterangan,
                'deskripsi_materi' => $value['deskripsi_materi'],
            ));
        }
        break;
    case "video":
        $data = $conn->query("SELECT * FROM event_materi where idevent = '$idevent' AND link_status = '3'");
        $datalist = array();
        foreach ($data as $key => $value) {
            if ($value['link_status'] = '1') {
                $keterangan = 'file pdf/books';
            } else if ($value['link_status'] = '3') {
                $keterangan = 'video';
            } else {
                $keterangan = 'file pdf/books';
            }
            array_push($datalist, array(
                'idassessment_materi' => $value['idassessment_materi'],
                'link_materi' => $value['link_materi'],
                'link_status' => $value['link_status'],
                'keterangan' => $keterangan,
                'deskripsi_materi' => $value['deskripsi_materi'],
            ));
        }
        break;
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $datalist;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'Tidak ada data ditampilkan.';
    $response->data = [];
    $response->json();
    die();
}

mysqli_close($conn);
