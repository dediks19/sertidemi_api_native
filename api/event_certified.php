<?php
require_once('../config/koneksi.php');
require('../sertifikat/fpdf.php');

$idtransaksi = $_GET['idtransaksi'];

$event = mysqli_query($conn, "SELECT a.nama_sertifikat, c.nama_event, c.event_mulai, c.url_sertifikat_depan_template, c.url_logo_sponsor FROM transaksi a 
LEFT JOIN transaksi_detail b On a.idtransaksi = b.idtransaksi
LEFT JOIN event c ON b.idevent = c.idevent WHERE a.idtransaksi = '$idtransaksi' GROUP BY a.idtransaksi;")->fetch_assoc();
$template_event = "../image/event/".$event['url_sertifikat_depan_template'];
$logo_sponsor = "../image/sponsor/".$event['url_logo_sponsor'];

//$name = text to be added, $x= x cordinate, $y = y coordinate, $a = alignment , $f= Font Name, $t = Bold / Italic, $s = Font Size, $r = Red, $g = Green Font color, $b = Blue Font Color
function AddText($pdf, $text, $x, $y, $a, $f, $t, $s, $r, $g, $b) {
$pdf->SetFont($f,$t,$s);	
$pdf->SetXY($x,$y);
$pdf->SetTextColor($r,$g,$b);
$pdf->Cell(0,10,$text,0,0,$a);	
}

$pdf = new FPDF('L','mm','custom_landscape');
$pdf->AddPage();
$pdf->SetFont('Arial','B', 16);
// $pdf->SetFontSize(0);
$pdf->SetCreator('Sertidemi');
// Add background image for PDF
$pdf->Image($template_event,0,0,0);	

$pdf->SetTitle('eSertifikat_'.$event['nama_sertifikat'].'_'.$event['nama_event']);
//Add a Name to the certificate
AddText($pdf,ucwords($event['nama_sertifikat']), 70,90, 'L', 'times','B',70,35,35,35);

$pdf->Output('D', 'eSertifikat_'.$event['nama_sertifikat'].'_'.$event['nama_event'].'.pdf');