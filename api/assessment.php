<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$tag                             = $_GET['tag'];
$limit                           = $_GET['limit'] ?? '';
$offset                          = $_GET['offset'] ?? '';
$q                               = $_GET['q'] ?? '';
$idassessment_kategori           = $_GET['idassessment_kategori'] ?? '';
$idassessment                    = $_GET['idassessment'] ?? '';

switch ($tag) {
    case "semua":
        if (empty($idassessment)) {
            if (empty($q)) {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment where idassessment_kategori = '$idassessment_kategori' AND tanggal_batas_assessment >= NOW() ORDER BY idassessment DESC LIMIT $offset, $limit");
            } else {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment where idassessment_kategori = '$idassessment_kategori' AND 
                nama_assessment LIKE '%$q%' AND tanggal_batas_assessment >= NOW() ORDER BY idassessment DESC LIMIT $offset, $limit");
            }

            foreach ($data as $key => $value) {
                if ($value['diskon_assessment'] == 0) {
                    $harga_diskon = 0;
                } else {
                    if ($value['status_diskon'] = '1'){
                        $harga_diskon = $value['harga_assessment'] - $value['harga_assessment'] * ($value['diskon_assessment'] / 100);
                    } else if ($value['status_diskon'] = '2'){
                        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
                    } else {
                        $harga_diskon = 0;
                    }
                }
                array_push($datalist, array(
                    'id' => $value['idassessment'],
                    'nama' => $value['nama_assessment'],
                    'mulai' => $value['tanggal_mulai_assessment'],
                    'selesai' => $value['tanggal_batas_assessment'],
                    'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                    'url_logo_sponsor' => $getimagesponsor . $value['url_logo_sponsor'],
                    'harga' => $value['harga_assessment'],
                    'diskon' => $value['diskon_assessment'],
                    'status_diskon' => $value['status_diskon'],
                    'harga_diskon' => (string)$harga_diskon,
                    'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
                    'status' => 'assessment',
                    'point_assessment' => $value['point_assessment']." ".$value['point_assessment_nama'],
                ));
            }

            if (isset($datalist[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $datalist;
                $response->json();
                die();
            } else {
                if ($offset == 0) {
                    $response->code = 200;
                    $response->message = 'Tidak ada data ditampilkan.';
                    $response->data = [];
                    $response->json();
                    die();
                } else {
                    $response->code = 200;
                    $response->message = 'found';
                    $response->data = [];
                    $response->json();
                    die();
                }
            }
        } else {
            $idassessment = $_GET['idassessment'];
            $data = mysqli_fetch_object($conn->query("SELECT * FROM assessment a LEFT JOIN user_bo b ON b.iduser_bo = a.iduser_bo LEFT JOIN kategori_user_bo c ON b.idkategori = c.idkategori WHERE a.idassessment = '$idassessment'"));

            $cekmateri = $conn->query("SELECT * FROM assessment_materi WHERE idassessment = '$idassessment'")->num_rows;
            if ($cekmateri > 0) {
                $status_materi = 'Y';
            } else {
                $status_materi = 'N';
            }

            $listsponsor = array();
            $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$idassessment'");
            foreach ($sponsores as $key => $value) {
                array_push($listsponsor, array(
                    'idsponsor' => $value['idsponsor'],
                    'idevent' => $value['idevent'],
                    'idassessment' => $value['idassessment'],
                    'nama_sponsor' => $value['nama_sponsor'],
                    'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
                    'urutan_sponsor' => $value['urutan_sponsor'],
                ));
            }

            if (($data->diskon_assessment) == 0) {
                $harga_diskon = 0;
            } else {
                if ($data->status_diskon == '1'){
                    $harga_diskon = $data->harga_assessment - $data->harga_assessment * ($data->diskon_assessment / 100);
                } else if ($data->status_diskon == '2'){
                    $harga_diskon = $data->harga_assessment - $data->diskon_assessment;
                } else {
                    $harga_diskon = 0;
                }
            }

            $data1['idassessment']          = $data->idassessment;
            $data1['nama_assessment'] = $data->nama_assessment;
            $data1['deskripsi_assessment'] = $data->deskripsi_assessment;
            $data1['tanggal_mulai_assessment'] = $data->tanggal_mulai_assessment;
            $data1['tanggal_batas_assessment'] = $data->tanggal_batas_assessment;
            $data1['url_sertifikat_depan_template'] = $getimagesertifikat . $data->url_sertifikat_depan_template;
            $data1['url_image_panjang'] = $getimageassessment . $data->url_image_panjang;
            $data1['url_image_kotak'] = $getimageassessment . $data->url_image_kotak;
            $data1['url_logo_sponsor'] = $getimagesponsor . $data->url_logo_sponsor;
            $data1['harga_assessment'] = $data->harga_assessment;
            $data1['status_diskon'] = $data->status_diskon;
            $data1['diskon_assessment'] = $data->diskon_assessment;
            $data1['harga_diskon'] = (string)$harga_diskon;
            $data1['tampil_assessment_pilgan'] = $data->tampil_assessment_pilgan;
            $data1['tampil_assessment_essay'] = $data->tampil_assessment_essay;
            $data1['penulis'] = $data->nama_kategori;
            $data1['status_materi'] = $status_materi;
            $data1['waktu_pengerjaan'] = (string)(60 * (int)$data->waktu_pengerjaan);
            $data1['sponsor'] = $listsponsor;
            $data1['point_assessment'] = $data->point_assessment." ".$data->point_assessment_nama;

            if ($data) {
                $response->code = 200;
                $response->message = 'success';
                $response->data = $data1;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = mysqli_error($conn);
                $response->data = [];
                $response->json();
                die();
            }
        }
        break;
    case "Certification":
        if (empty($idassessment)) {
            if (empty($q)) {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment a
                WHERE idassessment_kategori = '$idassessment_kategori' AND a.idassessment NOT IN(SELECT am.idassessment FROM assessment_materi am) AND tanggal_batas_assessment >= NOW() ORDER BY idassessment DESC LIMIT $offset, $limit");
            } else {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment a
                WHERE idassessment_kategori = '$idassessment_kategori' AND a.idassessment NOT IN(SELECT am.idassessment FROM assessment_materi am) AND a.nama_assessment LIKE '%$q%' AND tanggal_batas_assessment >= NOW() ORDER BY idassessment DESC LIMIT $offset, $limit");
            }

            foreach ($data as $key => $value) {
                if ($value['diskon_assessment'] == 0) {
                    $harga_diskon = 0;
                } else {
                    if ($value['status_diskon'] == '1'){
                        $harga_diskon = $value['harga_assessment'] - $value['harga_assessment'] * ($value['diskon_assessment'] / 100);
                    } else if ($value['status_diskon'] == '2'){
                        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
                    } else {
                        $harga_diskon = 0;
                    }
                }
                array_push($datalist, array(
                    'id' => $value['idassessment'],
                    'nama' => $value['nama_assessment'],
                    'mulai' => $value['tanggal_mulai_assessment'],
                    'selesai' => $value['tanggal_batas_assessment'],
                    'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                    'url_logo_sponsor' => $getimagesponsor . $value['url_logo_sponsor'],
                    'harga' => $value['harga_assessment'],
                    'diskon' => $value['diskon_assessment'],
                    'status_diskon' => $value['status_diskon'],
                    'harga_diskon' => (string)$harga_diskon,
                    'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
                    'status' => 'assessment',
                    'point_assessment' => $value['point_assessment']." ".$value['point_assessment_nama'],
                ));
            }

            if (isset($datalist[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $datalist;
                $response->json();
                die();
            } else {
                if ($offset == 0) {
                    $response->code = 200;
                    $response->message = 'Tidak ada data ditampilkan.';
                    $response->data = [];
                    $response->json();
                    die();
                } else {
                    $response->code = 200;
                    $response->message = 'found';
                    $response->data = [];
                    $response->json();
                    die();
                }
            }
        } else {
            $idassessment = $_GET['idassessment'];
            $data = mysqli_fetch_object($conn->query("SELECT * FROM assessment a LEFT JOIN user_bo b ON b.iduser_bo = a.iduser_bo LEFT JOIN kategori_user_bo c ON b.idkategori = c.idkategori WHERE a.idassessment = '$idassessment'"));

            $cekmateri = $conn->query("SELECT * FROM assessment_materi WHERE idassessment = '$idassessment'")->num_rows;
            if ($cekmateri > 0) {
                $status_materi = 'Y';
            } else {
                $status_materi = 'N';
            }

            $listsponsor = array();
            $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$idassessment'");
            foreach ($sponsores as $key => $value) {
                array_push($listsponsor, array(
                    'idsponsor' => $value['idsponsor'],
                    'idevent' => $value['idevent'],
                    'idassessment' => $value['idassessment'],
                    'nama_sponsor' => $value['nama_sponsor'],
                    'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
                    'urutan_sponsor' => $value['urutan_sponsor'],
                ));
            }

            if (($data->diskon_assessment) == 0) {
                $harga_diskon = 0;
            } else {
                if ($data->status_diskon == '1'){
                    $harga_diskon = $data->harga_assessment - $data->harga_assessment * ($data->diskon_assessment / 100);
                } else if ($data->status_diskon == '2'){
                    $harga_diskon = $data->harga_assessment - $data->diskon_assessment;
                } else {
                    $harga_diskon = 0;
                }
            }

            $data1['idassessment']          = $data->idassessment;
            $data1['nama_assessment'] = $data->nama_assessment;
            $data1['deskripsi_assessment'] = $data->deskripsi_assessment;
            $data1['tanggal_mulai_assessment'] = $data->tanggal_mulai_assessment;
            $data1['tanggal_batas_assessment'] = $data->tanggal_batas_assessment;
            $data1['url_sertifikat_depan_template'] = $getimagesertifikat . $data->url_sertifikat_depan_template;
            $data1['url_image_panjang'] = $getimageassessment . $data->url_image_panjang;
            $data1['url_image_kotak'] = $getimageassessment . $data->url_image_kotak;
            $data1['url_logo_sponsor'] = $getimagesponsor . $data->url_logo_sponsor;
            $data1['harga_assessment'] = $data->harga_assessment;
            $data1['diskon_assessment'] = $data->diskon_assessment;
            $data1['status_diskon'] = $data->status_diskon;
            $data1['harga_diskon'] = (string)$harga_diskon;
            $data1['tampil_assessment_pilgan'] = $data->tampil_assessment_pilgan;
            $data1['tampil_assessment_essay'] = $data->tampil_assessment_essay;
            $data1['penulis'] = $data->nama_kategori;
            $data1['status_materi'] = $status_materi;
            $data1['waktu_pengerjaan'] = (string)(60 * (int)$data->waktu_pengerjaan);
            $data1['sponsor'] = $listsponsor;
            $data1['point_assessment'] = $data->point_assessment." ".$data->point_assessment_nama;

            if ($data) {
                $response->code = 200;
                $response->message = 'success';
                $response->data = $data1;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = mysqli_error($conn);
                $response->data = [];
                $response->json();
                die();
            }
        }
        break;
    case "Text+File":
        if (empty($idassessment)) {
            if (empty($q)) {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment a JOIN assessment_materi b  ON b.idassessment = a.idassessment WHERE a.idassessment_kategori = '$idassessment_kategori' AND (b.link_status = '1' OR b.link_status = '2') AND a.tanggal_batas_assessment >= NOW() GROUP BY a.idassessment ORDER BY a.idassessment DESC LIMIT $offset, $limit");
            } else {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment a JOIN assessment_materi b ON b.idassessment = a.idassessment WHERE a.idassessment_kategori = '$idassessment_kategori' AND 
                a.nama_assessment LIKE '%$q%' AND a.tanggal_batas_assessment >= NOW() GROUP BY a.idassessment ORDER BY a.idassessment DESC LIMIT $offset, $limit");
            }

            foreach ($data as $key => $value) {
                if ($value['diskon_assessment'] == 0) {
                    $harga_diskon = 0;
                } else {
                    if ($value['status_diskon'] == '1'){
                        $harga_diskon = $value['harga_assessment'] - $value['harga_assessment'] * ($value['diskon_assessment'] / 100);
                    } else if ($value['status_diskon'] == '2'){
                        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
                    } else {
                        $harga_diskon = 0;
                    }
                }
                array_push($datalist, array(
                    'id' => $value['idassessment'],
                    'nama' => $value['nama_assessment'],
                    'mulai' => $value['tanggal_mulai_assessment'],
                    'selesai' => $value['tanggal_batas_assessment'],
                    'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                    'url_logo_sponsor' => $getimagesponsor . $value['url_logo_sponsor'],
                    'harga' => $value['harga_assessment'],
                    'diskon' => $value['diskon_assessment'],
                    'status_diskon' => $value['status_diskon'],
                    'harga_diskon' => (string)$harga_diskon,
                    'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
                    'status' => 'assessment',
                    'point_assessment' => $value['point_assessment']." ".$value['point_assessment_nama'],
                ));
            }

            if (isset($datalist[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $datalist;
                $response->json();
                die();
            } else {
                if ($offset == 0) {
                    $response->code = 200;
                    $response->message = 'Tidak ada data ditampilkan.';
                    $response->data = [];
                    $response->json();
                    die();
                } else {
                    $response->code = 200;
                    $response->message = 'found';
                    $response->data = [];
                    $response->json();
                    die();
                }
            }
        } else {
            $idassessment = $_GET['idassessment'];
            $data = mysqli_fetch_object($conn->query("SELECT * FROM assessment a LEFT JOIN user_bo b ON b.iduser_bo = a.iduser_bo LEFT JOIN kategori_user_bo c ON b.idkategori = c.idkategori WHERE a.idassessment = '$idassessment'"));

            $cekmateri = $conn->query("SELECT * FROM assessment_materi WHERE idassessment = '$idassessment'")->num_rows;
            if ($cekmateri > 0) {
                $status_materi = 'Y';
            } else {
                $status_materi = 'N';
            }

            $listsponsor = array();
            $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$idassessment'");
            foreach ($sponsores as $key => $value) {
                array_push($listsponsor, array(
                    'idsponsor' => $value['idsponsor'],
                    'idevent' => $value['idevent'],
                    'idassessment' => $value['idassessment'],
                    'nama_sponsor' => $value['nama_sponsor'],
                    'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
                    'urutan_sponsor' => $value['urutan_sponsor'],
                ));
            }

            if (($data->diskon_assessment) == 0) {
                $harga_diskon = 0;
            } else {
                if ($data->status_diskon == '1'){
                    $harga_diskon = $data->harga_assessment - $data->harga_assessment * ($data->diskon_assessment / 100);
                } else if ($data->status_diskon == '2'){
                    $harga_diskon = $data->harga_assessment - $data->diskon_assessment;
                } else {
                    $harga_diskon = 0;
                }
            }

            $data1['idassessment']          = $data->idassessment;
            $data1['nama_assessment'] = $data->nama_assessment;
            $data1['deskripsi_assessment'] = $data->deskripsi_assessment;
            $data1['tanggal_mulai_assessment'] = $data->tanggal_mulai_assessment;
            $data1['tanggal_batas_assessment'] = $data->tanggal_batas_assessment;
            $data1['url_sertifikat_depan_template'] = $getimagesertifikat . $data->url_sertifikat_depan_template;
            $data1['url_image_panjang'] = $getimageassessment . $data->url_image_panjang;
            $data1['url_image_kotak'] = $getimageassessment . $data->url_image_kotak;
            $data1['url_logo_sponsor'] = $getimagesponsor . $data->url_logo_sponsor;
            $data1['harga_assessment'] = $data->harga_assessment;
            $data1['diskon_assessment'] = $data->diskon_assessment;
            $data1['status_diskon'] = $data->status_diskon;
            $data1['harga_diskon'] = (string)$harga_diskon;
            $data1['tampil_assessment_pilgan'] = $data->tampil_assessment_pilgan;
            $data1['tampil_assessment_essay'] = $data->tampil_assessment_essay;
            $data1['penulis'] = $data->nama_kategori;
            $data1['status_materi'] = $status_materi;
            $data1['waktu_pengerjaan'] = (string)(60 * (int)$data->waktu_pengerjaan);
            $data1['sponsor'] = $listsponsor;
            $data1['point_assessment'] = $data->point_assessment." ".$data->point_assessment_nama;

            if ($data) {
                $response->code = 200;
                $response->message = 'success';
                $response->data = $data1;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = mysqli_error($conn);
                $response->data = [];
                $response->json();
                die();
            }
        }
        break;
    case "video":
        if (empty($idassessment)) {
            if (empty($q)) {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment a JOIN assessment_materi b  ON b.idassessment = a.idassessment WHERE a.idassessment_kategori = '$idassessment_kategori' AND b.link_status = '3' AND a.tanggal_batas_assessment >= NOW() GROUP BY a.idassessment ORDER BY a.idassessment DESC LIMIT $offset, $limit");
            } else {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment a JOIN assessment_materi b ON b.idassessment = a.idassessment WHERE a.idassessment_kategori = '$idassessment_kategori' AND 
                a.nama_assessment LIKE '%$q%' AND a.tanggal_batas_assessment >= NOW() GROUP BY a.idassessment ORDER BY a.idassessment DESC LIMIT $offset, $limit");
            }

            foreach ($data as $key => $value) {
                if ($value['diskon_assessment'] == 0) {
                    $harga_diskon = 0;
                } else {
                    if ($value['status_diskon'] == '1'){
                        $harga_diskon = $value['harga_assessment'] - $value['harga_assessment'] * ($value['diskon_assessment'] / 100);
                    } else if ($value['status_diskon'] == '2'){
                        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
                    } else {
                        $harga_diskon = 0;
                    }
                }
                array_push($datalist, array(
                    'id' => $value['idassessment'],
                    'nama' => $value['nama_assessment'],
                    'mulai' => $value['tanggal_mulai_assessment'],
                    'selesai' => $value['tanggal_batas_assessment'],
                    'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                    'url_logo_sponsor' => $getimagesponsor . $value['url_logo_sponsor'],
                    'harga' => $value['harga_assessment'],
                    'diskon' => $value['diskon_assessment'],
                    'status_diskon' => $value['status_diskon'],
                    'harga_diskon' => (string)$harga_diskon,
                    'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
                    'status' => 'assessment',
                    'point_assessment' => $value['point_assessment']." ".$value['point_assessment_nama'],
                ));
            }

            if (isset($datalist[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $datalist;
                $response->json();
                die();
            } else {
                if ($offset == 0) {
                    $response->code = 200;
                    $response->message = 'Tidak ada data ditampilkan.';
                    $response->data = [];
                    $response->json();
                    die();
                } else {
                    $response->code = 200;
                    $response->message = 'found';
                    $response->data = [];
                    $response->json();
                    die();
                }
            }
        } else {
            $idassessment = $_GET['idassessment'];
            $data = mysqli_fetch_object($conn->query("SELECT * FROM assessment a LEFT JOIN user_bo b ON b.iduser_bo = a.iduser_bo LEFT JOIN kategori_user_bo c ON b.idkategori = c.idkategori WHERE a.idassessment = '$idassessment'"));

            $cekmateri = $conn->query("SELECT * FROM assessment_materi WHERE idassessment = '$idassessment'")->num_rows;
            if ($cekmateri > 0) {
                $status_materi = 'Y';
            } else {
                $status_materi = 'N';
            }

            $listsponsor = array();
            $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$idassessment'");
            foreach ($sponsores as $key => $value) {
                array_push($listsponsor, array(
                    'idsponsor' => $value['idsponsor'],
                    'idevent' => $value['idevent'],
                    'idassessment' => $value['idassessment'],
                    'nama_sponsor' => $value['nama_sponsor'],
                    'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
                    'urutan_sponsor' => $value['urutan_sponsor'],
                ));
            }

            if (($data->diskon_assessment) == 0) {
                $harga_diskon = 0;
            } else {
                if ($data->status_diskon == '1'){
                    $harga_diskon = $data->harga_assessment - $data->harga_assessment * ($data->diskon_assessment / 100);
                } else if ($data->status_diskon == '2'){
                    $harga_diskon = $data->harga_assessment - $data->diskon_assessment;
                } else {
                    $harga_diskon = 0;
                }
            }

            $data1['idassessment']          = $data->idassessment;
            $data1['nama_assessment'] = $data->nama_assessment;
            $data1['deskripsi_assessment'] = $data->deskripsi_assessment;
            $data1['tanggal_mulai_assessment'] = $data->tanggal_mulai_assessment;
            $data1['tanggal_batas_assessment'] = $data->tanggal_batas_assessment;
            $data1['url_sertifikat_depan_template'] = $getimagesertifikat . $data->url_sertifikat_depan_template;
            $data1['url_image_panjang'] = $getimageassessment . $data->url_image_panjang;
            $data1['url_image_kotak'] = $getimageassessment . $data->url_image_kotak;
            $data1['url_logo_sponsor'] = $getimagesponsor . $data->url_logo_sponsor;
            $data1['harga_assessment'] = $data->harga_assessment;
            $data1['diskon_assessment'] = $data->diskon_assessment;
            $data1['status_diskon'] = $data->status_diskon;
            $data1['harga_diskon'] = (string)$harga_diskon;
            $data1['tampil_assessment_pilgan'] = $data->tampil_assessment_pilgan;
            $data1['tampil_assessment_essay'] = $data->tampil_assessment_essay;
            $data1['penulis'] = $data->nama_kategori;
            $data1['status_materi'] = $status_materi;
            $data1['waktu_pengerjaan'] = (string)(60 * (int)$data->waktu_pengerjaan);
            $data1['sponsor'] = $listsponsor;
            $data1['point_assessment'] = $data->point_assessment." ".$data->point_assessment_nama;

            if ($data) {
                $response->code = 200;
                $response->message = 'success';
                $response->data = $data1;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = mysqli_error($conn);
                $response->data = [];
                $response->json();
                die();
            }
        }
        break;
    default:
        if (empty($idassessment)) {
            if (empty($q)) {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment where idassessment_kategori = '$idassessment_kategori' AND tanggal_batas_assessment >= NOW() ORDER BY idassessment DESC LIMIT $offset, $limit");
            } else {
                $datalist = array();
                $data = $conn->query("SELECT * FROM assessment where idassessment_kategori = '$idassessment_kategori' AND 
                    nama_assessment LIKE '%$q%' AND tanggal_batas_assessment >= NOW() ORDER BY idassessment DESC LIMIT $offset, $limit");
            }

            foreach ($data as $key => $value) {
                if ($value['diskon_assessment'] == 0) {
                    $harga_diskon = 0;
                } else {
                    if ($value['status_diskon'] == '1'){
                        $harga_diskon = $value['harga_assessment'] - $value['harga_assessment'] * ($value['diskon_assessment'] / 100);
                    } else if ($value['status_diskon'] == '2'){
                        $harga_diskon = $value['harga_assessment'] - $value['diskon_assessment'];
                    } else {
                        $harga_diskon = 0;
                    }
                }
                array_push($datalist, array(
                    'id' => $value['idassessment'],
                    'nama' => $value['nama_assessment'],
                    'mulai' => $value['tanggal_mulai_assessment'],
                    'selesai' => $value['tanggal_batas_assessment'],
                    'url_image_panjang' => $getimageassessment . $value['url_image_panjang'],
                    'url_logo_sponsor' => $getimagesponsor . $value['url_logo_sponsor'],
                    'harga' => $value['harga_assessment'],
                    'diskon' => $value['diskon_assessment'],
                    'status_diskon' => $value['status_diskon'],
                    'harga_diskon' => (string)$harga_diskon,
                    'sponsor_utama' => $getimageassessment . $value['url_logo_sponsor'],
                    'status' => 'assessment',
                    'point_assessment' => $value['point_assessment']." ".$value['point_assessment_nama'],
                ));
            }

            if (isset($datalist[0])) {
                $response->code = 200;
                $response->message = 'result';
                $response->data = $datalist;
                $response->json();
                die();
            } else {
                if ($offset == 0) {
                    $response->code = 200;
                    $response->message = 'Tidak ada data ditampilkan.';
                    $response->data = [];
                    $response->json();
                    die();
                } else {
                    $response->code = 200;
                    $response->message = 'found';
                    $response->data = [];
                    $response->json();
                    die();
                }
            }
        } else {
            $idassessment = $_GET['idassessment'];
            $data = mysqli_fetch_object($conn->query("SELECT * FROM assessment a LEFT JOIN user_bo b ON b.iduser_bo = a.iduser_bo LEFT JOIN kategori_user_bo c ON b.idkategori = c.idkategori WHERE a.idassessment = '$idassessment'"));

            $cekmateri = $conn->query("SELECT * FROM assessment_materi WHERE idassessment = '$idassessment'")->num_rows;
            if ($cekmateri > 0) {
                $status_materi = 'Y';
            } else {
                $status_materi = 'N';
            }

            $listsponsor = array();
            $sponsores = $conn->query("SELECT * FROM sponsor WHERE idassessment = '$idassessment'");
            foreach ($sponsores as $key => $value) {
                array_push($listsponsor, array(
                    'idsponsor' => $value['idsponsor'],
                    'idevent' => $value['idevent'],
                    'idassessment' => $value['idassessment'],
                    'nama_sponsor' => $value['nama_sponsor'],
                    'url_logo_sponsor' => $getimagesponsor . $value['icon_sponsor'],
                    'urutan_sponsor' => $value['urutan_sponsor'],
                ));
            }

            if (($data->diskon_assessment) == 0) {
                $harga_diskon = 0;
            } else {
                if ($data->status_diskon == '1'){
                    $harga_diskon = $data->harga_assessment - $data->harga_assessment * ($data->diskon_assessment / 100);
                } else if ($data->status_diskon == '2'){
                    $harga_diskon = $data->harga_assessment - $data->diskon_assessment;
                } else {
                    $harga_diskon = 0;
                }
            }

            $data1['idassessment']          = $data->idassessment;
            $data1['nama_assessment'] = $data->nama_assessment;
            $data1['deskripsi_assessment'] = $data->deskripsi_assessment;
            $data1['tanggal_mulai_assessment'] = $data->tanggal_mulai_assessment;
            $data1['tanggal_batas_assessment'] = $data->tanggal_batas_assessment;
            $data1['url_sertifikat_depan_template'] = $getimagesertifikat . $data->url_sertifikat_depan_template;
            $data1['url_image_panjang'] = $getimageassessment . $data->url_image_panjang;
            $data1['url_image_kotak'] = $getimageassessment . $data->url_image_kotak;
            $data1['url_logo_sponsor'] = $getimagesponsor . $data->url_logo_sponsor;
            $data1['harga_assessment'] = $data->harga_assessment;
            $data1['diskon_assessment'] = $data->diskon_assessment;
            $data1['harga_diskon'] = (string)$harga_diskon;
            $data1['status_diskon'] = $data->status_diskon;
            $data1['tampil_assessment_pilgan'] = $data->tampil_assessment_pilgan;
            $data1['tampil_assessment_essay'] = $data->tampil_assessment_essay;
            $data1['penulis'] = $data->nama_kategori;
            $data1['status_materi'] = $status_materi;
            $data1['waktu_pengerjaan'] = (string)(60 * (int)$data->waktu_pengerjaan);
            $data1['sponsor'] = $listsponsor;
            $data1['point_assessment'] = $data->point_assessment." ".$data->point_assessment_nama;

            if ($data) {
                $response->code = 200;
                $response->message = 'success';
                $response->data = $data1;
                $response->json();
                die();
            } else {
                $response->code = 200;
                $response->message = mysqli_error($conn);
                $response->data = [];
                $response->json();
                die();
            }
        }
        break;
}

mysqli_close($conn);
