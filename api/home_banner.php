<?php
require_once('../config/koneksi.php');
include "response.php";

$response = new Response();

$data = $conn->query("SELECT * FROM banner WHERE status_aktif = 'Y'");
$datalist = array();
foreach ($data as $key => $value) {
    array_push($datalist, array(
        'idbanner' => $value['idbanner'],
        'nama_banner' => $value['nama_banner'],
        'tgl_posting_banner' => date('d F Y h:i:s A', strtotime($value['tgl_posting_banner'])),
        'tgl_exp_banner' => date('d F Y h:i:s A', strtotime($value['tgl_exp_banner'])),
        'file_banner' => $getimagebanner.$value['file_banner'],
        'deskripsi_banner' => $value['deskripsi_banner'],
        'link_banner' => $value['link_banner'],
    ));  
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $datalist;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'Tidak ada data ditampilkan.';
    $response->data = '';
    $response->json();
    die();
}

mysqli_close($conn);
