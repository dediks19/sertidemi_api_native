<?php
require_once('../config/koneksi.php');
include "response.php";


$id = $_GET['id'] ?? SendError();
$search = $_GET['q'] ?? '';
$idmaster = $_GET['idmaster'] ?? SendError();

$response = new Response();
switch ($id) {
	case 'event':
		$data = $conn->query("SELECT * FROM event_kategori WHERE nama_kategori LIKE '%$search%' AND idevent_kategori_master = '$idmaster';");
		
		$listdata = array();
            // $event = $conn->query("SELECT * FROM event_kategori_master");
            foreach ($data as $key => $value) {
                array_push($listdata, array(
                    'dievent_kategori' => $value['dievent_kategori'],
                    'idevent_kategori_master' => $value['idevent_kategori_master'],
                    'nama_kategori' => $value['nama_kategori'],
                    'icon' => $getkategoriiconsub . $value['icon'],
                ));
        }

		if (isset($listdata[0])) {
// 			$data = $data->fetch_all(MYSQLI_ASSOC);
			$response->code = 200;
			$response->message = 'found';
			$response->data = $listdata;
		} else {
			$response->code = 200;
			$response->message = mysqli_error($conn);
			$response->data = [];
		}
		break;

	case 'assessment':
		$data = $conn->query("SELECT * FROM assessment_kategori where nama_kategori LIKE '%$search%' AND idassessment_kategori_master = '$idmaster';");
			$listdata = array();
            // $event = $conn->query("SELECT * FROM event_kategori_master");
            foreach ($data as $key => $value) {
                array_push($listdata, array(
                    'idassessment_kategori' => $value['idassessment_kategori'],
                    'idassessment_kategori_master' => $value['idassessment_kategori_master'],
                    'nama_kategori' => $value['nama_kategori'],
                    'icon' => $getkategoriiconsub . $value['icon'],
                ));
        }

		if (isset($listdata[0])) {
// 			$data = $data->fetch_all(MYSQLI_ASSOC);
			$response->code = 200;
			$response->message = 'found';
			$response->data = $listdata;
		} else {
			$response->code = 200;
			$response->message = mysqli_error($conn);
			$response->data = [];
		}
		break;

	default:
		$response->code = 400;
		$response->message = 'bad request';
		$response->data = '';
		break;
}
$response->json();
die();

function sendError()
{
	$response = new Response();
	$response->code = 400;
	$response->message = 'bad request';
	$response->data = '';
	$response->json();
	die();
}
