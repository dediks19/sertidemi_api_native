<?php
require_once('../config/koneksi.php');
include "response.php";

$id = $_GET['id'] ?? '';

$response = new Response();
if (empty($id)) {
    $response->code = 400;
    $response->message = 'bad request';
    $response->data = '';
    $response->json();
    die();
} else {
    $datalist = array();
    $data = $conn->query("SELECT * FROM sponsor WHERE (idevent = '$id' OR idassessment = '$id') ORDER BY urutan_sponsor ASC");
    foreach ($data as $key => $value) {
        array_push($datalist, array(
            'idsponsor' => $value['idsponsor'],
            'nama_sponsor' => $value['nama_sponsor'],
            'icon_sponsor' => $value['icon_sponsor'],
        ));
    }

    if (isset($datalist[0])) {
        $response->code = 200;
        $response->message = 'result';
        $response->data = $datalist;
        $response->json();
        die();
    } else {
        $response->code = 200;
        $response->message = 'Tidak ada data ditampilkan.';
        $response->data = [];
        $response->json();
        die();
    }
}
