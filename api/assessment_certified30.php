<?php
require_once('../config/koneksi.php');
require('../sertifikat/fpdf.php');

$idtransaksi = $_GET['idtransaksi'];

$cekassessment = mysqli_query($conn, "SELECT * FROM transaksi_detail a JOIN assessment b ON b.idassessment = a.idassessment WHERE a.idtransaksi = '$idtransaksi'")->fetch_assoc();
if (($cekassessment['tampil_assessment_pilgan'] != 0) && ($cekassessment['tampil_assessment_essay'] == 0)) {
    $niasa = 1;
} else if (($cekassessment['tampil_assessment_pilgan'] == 0) && ($cekassessment['tampil_assessment_essay'] != 0)) {
    $niasa = 2;
}

if ($niasa == 1) {
    $assessment = mysqli_query($conn, "SELECT a.nama_sertifikat, c.nama_assessment, c.url_sertifikat_depan_template, c.url_sertifikat_belakang_template, d.nilai FROM transaksi a 
    LEFT JOIN transaksi_detail b On a.idtransaksi = b.idtransaksi
    LEFT JOIN assessment c ON b.idassessment = c.idassessment
    LEFT JOIN assessment_jawaban_pilgan d ON c.idassessment = d.idassessment WHERE a.idtransaksi = '$idtransaksi' GROUP BY a.idtransaksi")->fetch_assoc();
} else if ($niasa == 2) {
    $assessment = mysqli_query($conn, "SELECT b.nama_sertifikat, c.nama_assessment, c.url_sertifikat_depan_template, c.url_sertifikat_belakang_template, d.nilai FROM assessment_sertifikat a 
    LEFT JOIN transaksi b ON a.idtransaksi = b.idtransaksi
    LEFT JOIN assessment c On a.idassessment = c.idassessment
    LEFT JOIN assessment_jawaban_essay d ON b.idtransaksi = d.idtransaksi
    WHERE d.koreksi_admin = '2' AND d.idtransaksi = '$idtransaksi'")->fetch_assoc();
}

$template_event = $getimageassessment.$assessment['url_sertifikat_depan_template'];
$template_event_belakang = $getimageassessment.$assessment['url_sertifikat_belakang_template'];
// $logo_sponsor = "../image/sponsor/".$assessment['url_logo_sponsor'];
if ($assessment['nilai'] <= 30) {
    $ket_nilai = $assessment['nilai'];
} else {
    $ket_nilai = $assessment['nilai'];
}

if ($assessment['nilai'] <= 30) {
    $ket_predikat = 'PARTICIPANT';
} else if ($assessment['nilai'] <= 40) {
    $ket_predikat = 'BEGINNER';
} else if ($assessment['nilai'] <= 50) {
    $ket_predikat = 'PIONEER';
} else if ($assessment['nilai'] <= 60) {
    $ket_predikat = 'ADVANCED BEGINNERS';
} else if ($assessment['nilai'] <= 70) {
    $ket_predikat = 'COMPETENT';
} else if ($assessment['nilai'] <= 80) {
    $ket_predikat = 'PROFESSIONAL';
} else if ($assessment['nilai'] <= 90) {
    $ket_predikat = 'EXPERT';
} else {
    $ket_predikat = 'MASTER';
}

//$name = text to be added, $x= x cordinate, $y = y coordinate, $a = alignment , $f= Font Name, $t = Bold / Italic, $s = Font Size, $r = Red, $g = Green Font color, $b = Blue Font Color
function AddText($pdf, $text, $x, $y, $a, $f, $t, $s, $r, $g, $b)
{
    $pdf->SetFont($f, $t, $s);
    $pdf->SetXY($x, $y);
    $pdf->SetTextColor($r, $g, $b);
    $pdf->Cell(0, 10, $text, 0, 0, $a);
}

$pdf = new FPDF('P', 'mm', 'custom3');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);
// $pdf->SetFontSize(0);
$pdf->SetCreator('Sertidemi');
// Add background image for PDF
$pdf->Image($template_event, 0, 0, 0);
//SETTINGAN DEFAULT CUSTOM2
// AddText($pdf, ucwords($assessment['nama_sertifikat']), 78, 185, 'L', 'times', 'B', 35, 35, 35, 35);
// AddText($pdf, ucwords("Score " . $ket_nilai . " - (" . $ket_predikat . ")"), 78, 200, 'L', 'times', 'B', 22, 35, 35, 35);
AddText($pdf, ucwords($assessment['nama_sertifikat']), 10, 165, 'C', 'times', 'B', 40, 35, 35, 35);
AddText($pdf, ucwords("Score " . $ket_nilai . " - (" . $ket_predikat . ")"), 110, 195, 'L', 'times', 'B', 22, 35, 35, 35);
//BIAR NAMBAH PAGE BARU
for ($i = 1; $i <= 100; $i++) {
    $pdf->MultiCell(0, 5, '');
}
$pdf->SetTitle('eSertifikat_' . $assessment['nama_sertifikat'] . '_' . $assessment['nama_assessment']);

$pdf->Image($template_event_belakang, 0, 0, 0);
//Add a Name to the certificate
$pdf->Output('I', 'eSertifikat_' . $assessment['nama_sertifikat'] . '_' . $assessment['nama_assessment'] . '.pdf');
