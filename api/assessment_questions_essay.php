<?php
require_once('../config/koneksi.php');
include "response.php";
$response = new Response();

$idassessment = $_GET['idassessment'] ?? '';

$cek_assessment_essay = $conn->query("SELECT * FROM assessment WHERE idassessment = '$idassessment'")->fetch_assoc();
$tampil_assessment = $cek_assessment_essay['tampil_assessment_essay'];

$data = $conn->query("SELECT * FROM assessment_soal_essay WHERE idassessment = '$idassessment'");
$datalist = array();
while ($row = mysqli_fetch_array($data)) {
    array_push($datalist, array(
        'idassessment_soal_essay' => $row['idassessment_soal_essay'],
        'pertanyaan_text' => $row['pertanyaan_text'],
        'pertanyaan_gambar' => $row['pertanyaan_gambar'],
    ));
}  
shuffle($datalist);
$datalist2 = array();

for ($i = 0; $i < $tampil_assessment; $i++) {
    array_push($datalist2, $datalist[$i]);
}

if (isset($datalist[0])) {
    $response->code = 200;
    $response->message = 'result';
    $response->data = $datalist2;
    $response->json();
    die();
} else {
    $response->code = 200;
    $response->message = 'Tidak ada data ditampilkan.';
    $response->data = [];
    $response->json();
    die();
}
